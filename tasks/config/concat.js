'use strict';

module.exports = function(grunt) {
  grunt.config.set('concat', {
    js: {
      src: [
        '.tmp/app.js',
        '.tmp/*.js',
        '.tmp/services/**/*.js',
        '.tmp/directives/**/*.js',
        '.tmp/filters/**/*.js',
        '.tmp/modules/shared/**/*.js',
        '.tmp/modules/!(shared)/**/*.js',
      ],
      dest: '.tmp/main.js',
    },
    css: {
      src: [
        '.tmp/modules/shared/**/*.css',
        '.tmp/modules/!(shared)/**/*.css',
      ],
      dest: '.tmp/main.css',
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
};
