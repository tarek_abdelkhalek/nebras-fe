'use strict';

module.exports = function(grunt) {
  grunt.config.set('injector', {
    options: {
      ignorePath: '.tmp',
      addRootSlash: true
    },
    local_dependencies: {
      files: {
        '.tmp/index.html': [
          [
            '.tmp/env.js',
            '.tmp/app.js',
            '.tmp/!(env)*.js',

            '.tmp/services/**/*.js',
            '.tmp/directives/**/*.js',
            '.tmp/filters/**/*.js',

            '.tmp/modules/shared/**/*.js',
            '.tmp/modules/!(shared)/module/module.js',
            '.tmp/modules/!(shared)/module/!(module).js',
            '.tmp/modules/!(shared)/!(module)/*.js',
          ],
          [
            '.tmp/modules/shared/**/*.css',
            '.tmp/modules/!(shared)/**/*.css',
          ]
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-injector');
};
