(function () {
  'use strict';

  // used in files uploading
  // ng-model => files-model

  angular.module('app')
    .directive('filesModel', filesModel);

  filesModel.$inject = ['$parse'];
  function filesModel($parse) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        var model = $parse(attrs.filesModel);
        var modelSetter = model.assign;

        element.bind('change', function () {
          scope.$apply(function () {
            modelSetter(scope, element[0].files);
          });
        });
      }
    };
  }
})();
