(function () {
  'use strict';

  // used in pictures with headers (for auth)

  angular.module('app')
    .directive('httpSrc', httpSrc);

  httpSrc.$inject = ['$http'];
  function httpSrc($http) {

    return {
      link: function ($scope, elem, attrs) {
        function revokeObjectURL() {
          if ($scope.objectURL) {
            URL.revokeObjectURL($scope.objectURL);
          }
        }

        $scope.$watch('objectURL', function (objectURL) {
          elem.attr('src', objectURL);
        });

        $scope.$on('$destroy', function () {
          revokeObjectURL();
        });

        attrs.$observe('httpSrc', function (url) {
          revokeObjectURL();

          if(url && url.indexOf('[') === -1) {
            $http.get(url, { responseType: 'arraybuffer' })
            .then(function (response) {
              var blob = new Blob(
                [ response.data ],
                { type: response.headers('Content-Type') }
              );
              $scope.objectURL = URL.createObjectURL(blob);
            });
          }
        });
      }
    };

  }
})();
