(function () {
  'use strict';

  angular.module('app')
    .directive('statisticsModel', statisticsModel);

  statisticsModel.$inject = [];

  function statisticsModel() {
    return {
      restrict: 'E',
      templateUrl: 'directives/templates/statistics-model.html',
      replace: true,
      scope: {
        statisticObj: '='
      }
    };
  }
})();
