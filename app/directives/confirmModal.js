(function () {
  'use strict';

  angular.module('app')
    .directive('confirmModal', confirmModal);

  confirmModal.$inject = [];

  function confirmModal() {
    return {
      restrict: 'E',
      templateUrl: 'directives/templates/confirm-modal.html',
      replace: true,
      scope: {
        confirmObj: '='
      }
    };
  }
})();
