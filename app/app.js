(function () {
  'use strict';

  angular.module('app', [
    'ngSanitize',
    'ngAnimate',
    'ngMessages',
    'ui.router',
    'satellizer',
    'toastr',
    'angularMoment',
    'pascalprecht.translate',
    'chart.js',
    'FBAngular',
    'ngTable',
    'ngQuill',
    
    'app.management',
    'app.learn'
  ]);

})();
