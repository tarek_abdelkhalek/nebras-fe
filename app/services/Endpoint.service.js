(function () {
  'use strict';

  angular.module('app')
    .factory('EndpointService', EndpointService);

  EndpointService.$inject = ['env'];

  function EndpointService(env) {
    var methods = {
      fetch: fetch
    };

    var apiUrl = env.apiUrl;
    var endpoints = {
      getWelcome: function () {
        return {
          method: 'GET',
          url: apiUrl + '/'
        };
      },
      profile: function () {
        return {
          method: 'GET',
          url: apiUrl + '/profile/'
        };
      },
      getDepart: function (params) {
        return {
          method: 'GET',
          url: apiUrl + '/departments/' + setParams(params)
        };
      },
      delDepart: function (id) {
        return {
          method: 'DELETE',
          url: apiUrl + '/departments' + customParams(id)
        };
      },
      addDepart: function () {
        return {
          method: 'POST',
          url: apiUrl + '/departments'
        };
      },
      addStudent: function () {
        return {
          method: 'POST',
          url: apiUrl + '/students'
        };
      },
      updateDepart: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/departments' + customParams(id)
        };
      },
      updateStudent: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/students' + customParams(id)
        };
      },
      getCustomDepart: function (obj) {
        return {
          method: 'GET',
          url: apiUrl + '/departments' + customParams(obj) + setParams(obj.params)
        };
      },
      getOffice: function (params) {
        return {
          method: 'GET',
          url: apiUrl + '/offices/' + setParams(params)
        };
      },
      getCustomOffice: function (obj) {
        return {
          method: 'GET',
          url: apiUrl + '/offices' + customParams(obj) + setParams(obj.params)
        };
      },
      addOffice: function () {
        return {
          method: 'POST',
          url: apiUrl + '/offices'
        };
      },
      addSchool: function () {
        return {
          method: 'POST',
          url: apiUrl + '/schools'
        };
      },
      editContact: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/contacts' + customParams(id)
        };
      },
      updateOffice: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/offices' + customParams(id)
        };
      },
      updateSchool: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/schools' + customParams(id)
        };
      },
      addContact: function () {
        return {
          method: 'POST',
          url: apiUrl + '/contacts'
        };
      },
      getSchool: function (params) {
        return {
          method: 'GET',
          url: apiUrl + '/schools/' + setParams(params)
        };
      },
      getCustomSchool: function (obj) {
        return {
          method: 'GET',
          url: apiUrl + '/schools' + customParams(obj) + setParams(obj.params)
        };
      },
      getStudent: function (params) {
        return {
          method: 'GET',
          url: apiUrl + '/students/' + setParams(params)
        };
      },
      getCustomStudent: function (opts) {
        return {
          method: 'GET',
          url: apiUrl + '/students/' + opts.id
        };
      },
      tree: function () {
        return {
          method: 'GET',
          url: apiUrl + '/tree/'
        };
      },
      updateProfile: function () {
        return {
          method: 'PATCH',
          url: apiUrl + '/profile'
        };
      },
      departCompact: function (opt) {
        return {
          method: 'GET',
          url: apiUrl + '/departments' + compactOpt(opt) + '?compact'
        };
      },
      officeCompact: function (opt) {
        return {
          method: 'GET',
          url: apiUrl + '/offices' + compactOpt(opt) + '?compact'
        };
      },
      schoolCompact: function () {
        return {
          method: 'GET',
          url: apiUrl + '/schools?compact'
        };
      },
      getCourses: function () {
        return {
          method: 'GET',
          url: apiUrl + '/courses?groupBy=category'
        };
      },
      getCoursesCompact: function () {
        return {
          method: 'GET',
          url: apiUrl + '/courses?compact'
        };
      },
      getExams: function () {
        return {
          method: 'GET',
          url: apiUrl + '/exams'
        };
      },
      postQuestion: function (id) {
        return {
          method: 'POST',
          url: apiUrl + '/courses/' + id + '/questions'
        };
      },
      getQuestionBank: function (obj) {
        return {
          method: 'GET',
          url: apiUrl + '/courses' + customParams(obj) + 'questions' + setParams(obj.params)
        };
      },
      patchQuestionBank: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/questions/' + id
        };
      },
      deleteQuestionBank: function (id) {
        return {
          method: 'DELETE',
          url: apiUrl + '/questions/' + id
        };
      },
      patchAnswerBank: function (id) {
        return {
          method: 'PATCH',
          url: apiUrl + '/choices/' + id
        };
      },
      deletechoiceBank: function (id) {
        return {
          method: 'DELETE',
          url: apiUrl + '/choices/' + id
        };
      },
      getCustomCourse: function (opts) {
        return {
          method: 'GET',
          url: apiUrl + '/courses/' + opts.courseId
        };
      },
      getVideo: function (id) {
        return {
          method: 'GET',
          url: apiUrl + '/videos/' + id
        };
      },
      getComments: function (id) {
        return {
          method: 'GET',
          url: apiUrl + '/videos/' + id + '/comments?extended'
        };
      },
      watchedVideo: function (id) {
        return {
          method: 'PUT',
          url: apiUrl + '/videos/' + id + '/complete'
        };
      },
      unwatchedVideo: function (id) {
        return {
          method: 'DELETE',
          url: apiUrl + '/videos/' + id + '/complete'
        };
      },
      videoComment: function (id) {
        return {
          method: 'POST',
          url: apiUrl + '/videos/' + id + '/comments'
        };
      },
      commentReply: function (id) {
        return {
          method: 'POST',
          url: apiUrl + '/comments/' + id + '/replies'
        };
      },
    };

    function fetch(name, opts) {
      if (!endpoints[name]) {
        return {};
      }
      return endpoints[name](opts);
    }

    function setParams(params) {
      var fParam = '?';
      for (var row in params) {
        if (row) {
          fParam += row + '=';
          fParam += params[row];
          fParam += '&';
        }
      }
      return fParam.slice(0, fParam.length - 1);
    }

    function customParams(obj) {
      var state = '/';
      if (obj.url) {
        state += obj.url.id + '/';
        state += angular.isDefined(obj.url.state) ? obj.url.state : '';
      } else {
        state += obj.id;
      }
      return state;
    }

    function compactOpt(opts) {
      var url = '/';
      if (angular.isDefined(opts)) {
        url += opts.id ? opts.id + '/' : '';
        url += opts.child;
      }
      return url;
    }
    return methods;
  }
})();
