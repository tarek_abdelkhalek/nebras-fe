(function () {
  'use strict';

  angular.module('app')
    .factory('BEService', BEService);

  BEService.$inject = ['env', '$http', 'EndpointService'];
  function BEService(env, $http, EndpointService) {
    var methods = { call: call };

    function call(endpointName, endpointParams, data, progressCB) {
      var endpoint = EndpointService.fetch(endpointName, endpointParams);
      data = data || {};

      var objectType = Object.prototype.toString.call(data).slice(8, -1);
      var payload = null;
      if(objectType === 'FormData') {
        payload = data;
      } else if(objectType === 'Object') {
        payload = new FormData();
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            payload.append(key, data[key]);
          }
        }
      }

      return $http({
        method: endpoint.method,
        url: endpoint.url,
        headers: { 'Content-Type': undefined },
        transformRequest: angular.identity,
        data: payload,
        // eventHandlers: {
        //   progress: function(c) {
        //     console.log(c);
        //   }
        // },
        uploadEventHandlers: progressCB ? {
          progress: function (e) {
            progressCB(e);
          }
        } : null,
      });
    }

    return methods;
  }
})();
