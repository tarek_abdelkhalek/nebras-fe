(function () {
  'use strict';

  angular.module('app')
    .controller('DashboardCtrl', DashboardCtrl);

  DashboardCtrl.$inject = ['$rootScope', '$auth', '$window', '$translate', '$timeout', '$state', 'Fullscreen'];

  function DashboardCtrl($rootScope, $auth, $window, $translate, $timeout, $state, Fullscreen) {
    var vm = this;
    vm.logout = logout;
    vm.state = $state;
    vm.toggleFullScreen = toggleFullScreen;
    init();

    function init() {
      $timeout(function () {
        $('.ui.accordion').accordion();
        $('.ui.dropdown').dropdown();
      }, 200);
    }

    function logout() {
      $translate('LOGOUT_MSG').then(function (msg) {
        if (confirm(msg)) {
          $rootScope.loading = true;
          $auth.logout();
          $window.location = '/';
        }
      });
    }

    function toggleFullScreen() {
      if (Fullscreen.isEnabled()) {
        Fullscreen.cancel();

      } else {
        Fullscreen.all();

      }
    }
  }
})();
