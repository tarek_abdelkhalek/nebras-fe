(function () {
  'use strict';

  angular.module('app')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['$rootScope', '$auth', '$timeout', '$window'];
  function LoginCtrl($rootScope, $auth, $timeout, $window) {
    var vm = this;
    vm.credentials = { username: '', password: '' };
    vm.login = login;

    init();

    function init() {
      $rootScope.loading = false;
      $rootScope.showContent = true;
    }

    function login() {
      $rootScope.loading = true;
      $timeout(function () {
        $auth.login(vm.credentials)
        .then(function (res) {
          $window.location = '/';
        })
        .catch(function (err) {
          $rootScope.loading = false;
          vm.credentials.password = '';
          // [@TODO] handle error (401)
        });
      }, 500);
    }
  }
})();
