(function () {
  'use strict';

  angular.module('app')
    .config(routes);

  routes.$inject = ['$stateProvider'];
  function routes($stateProvider) {
    $stateProvider.state('login', {
      url: '/login',
      templateUrl: 'modules/shared/login/login.html',
      controller: 'LoginCtrl as ctrl',
      data: {}
    });
  }
})();
