(function () {
  'use strict';

  angular.module('app')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('account', {
      abstract: true,
      url: '',
      templateUrl: 'modules/shared/account/account.html',
      controller: 'accountCtrl as ctrl',
      data: {
        protected: true
      },
      resolve: {
        init: ['$rootScope', 'toastr', 'BEService', function ($rootScope, toastr, BEService) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          BEService.call('profile', null)
            .then(function (res) {
              $rootScope.profile = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'Error');
            });
        }]
      }
    });
  }
})();
