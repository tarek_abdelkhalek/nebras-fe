(function () {
  'use strict';

  angular.module('app')
    .controller('accountCtrl', accountCtrl);

  accountCtrl.$inject = ['$rootScope', '$auth', '$window', '$translate', '$timeout', '$state'];

  function accountCtrl($rootScope, $auth, $window, $translate, $timeout, $state) {
    var vm = this;
    vm.logout = logout;
    vm.state = $state;
    init();

    function init() {
      $timeout(function () {
        $('.ui.dropdown').dropdown();
      }, 200);
    }

    function logout() {
      $translate('LOGOUT_MSG').then(function (msg) {
        if (confirm(msg)) {
          $rootScope.loading = true;
          $auth.logout();
          $window.location = '/';
        }
      });
    }
  }
})();
