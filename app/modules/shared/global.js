// global js file contains jQuery calls and functions that are used globally
$(document).ready(function(){
  'use strict';


  $('.ui.secondary.pointing.menu .item').tab();

  var selectedTheme = $('#colorBox #colorful-theme li');
  selectedTheme.click(function() {
      $('link[href*="theme"]').attr('href', $(this).attr('data-value'));
  });


  var themeOpt = $('#colorBox #theme_opt li');
  themeOpt.click(function() {
      $('link[href*="optradical"]').attr('href', $(this).attr('data-value'));

  });
});
