(function () {
  'use strict';

  angular.module('app')
    .config(routes);

  routes.$inject = ['$stateProvider'];
  function routes($stateProvider) {
    $stateProvider.state('admin', {
      url: '/',
      controller: 'EntryCtrl as ctrl'
    });
  }
})();
