(function() {
  'use strict';

  angular.module('app')
    .controller('EntryCtrl', EntryCtrl);

  EntryCtrl.$inject = ['$auth', '$state'];

  function EntryCtrl($auth, $state) {
    var vm = this;

    init();

    function init() {
      if ($state.current.name !== 'admin') {
        return;
      }
      var authPayload = $auth.getPayload();
      if (typeof authPayload === 'undefined' ||
        typeof authPayload.userRole === 'undefined') {
        $auth.logout();
        $state.go('login', {}, {
          reload: true
        });
      } else if (authPayload.userRole === 'admin') {
        $state.go('dashboard.management.departs-gallery', {}, {
          reload: true
        });
      }else if (authPayload.userRole === 'student') {
        $state.go('account.learn.user', {}, {
          reload: true
        });
      } else if (authPayload.userRole === 'department') { 
        $state.go('dashboard.management.offices-gallery', {}, {
          reload: true
        });
      } else if (authPayload.userRole === 'office') {
        $state.go('dashboard.management.schools-gallery', {}, {
          reload: true
        });
      } else if (authPayload.userRole === 'school') {
        $state.go('dashboard.management.students-gallery', {}, {
          reload: true
        });
      }
    }
  }
})();
