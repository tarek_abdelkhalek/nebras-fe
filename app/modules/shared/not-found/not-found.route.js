(function () {
  'use strict';

  angular.module('app')
    .config(routes);

  routes.$inject = ['$stateProvider'];
  function routes($stateProvider) {
    $stateProvider.state('notFound', {
      url: '/404',
      templateUrl: 'modules/shared/not-found/not-found.html',
      controller: 'NotFoundCtrl as ctrl',
      data: {}
    });
  }
})();
