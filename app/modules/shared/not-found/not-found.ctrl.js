(function () {
  'use strict';

  angular.module('app')
    .controller('NotFoundCtrl', NotFoundCtrl);

  NotFoundCtrl.$inject = ['$rootScope'];
  function NotFoundCtrl($rootScope) {
    var vm = this;

    init();

    function init() {
      $rootScope.loading = false;
      $rootScope.showContent = true;
    }
  }
})();
