(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.tree', {
      url: '/tree',
      templateUrl: 'modules/management/tree/tree.html',
      controller: 'treeCtrl as ctrl',
      data: {}
    });
  }
})();
