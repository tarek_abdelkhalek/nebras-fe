(function () {
  'use strict';

  angular.module('app.management')
    .controller('treeCtrl', treeCtrl);

  treeCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService', 'NgTableParams'];

  function treeCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService, NgTableParams) {
    var vm = this;
    init();

    function init() {
      console.log($rootScope.userRole);
      if ($rootScope.userRole !== 'admin') {
        $state.go('notFound');
      }
      BEService.call('tree')
        .then(function (res) {
          vm.tree = res.data;
          $rootScope.loading = false;
          $rootScope.showContent = true;
          console.log(vm.tree);
          //jquery code
          $timeout(function () {
            $('.ui.styled.fluid.accordion').accordion();
          }, 300);
        })
        .catch(function (err) {
          console.log(err);
        });
    }
  }
})();
