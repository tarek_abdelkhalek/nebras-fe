(function() {
  'use strict';

  angular.module('app.management')
    .controller('contractCtrl', contractCtrl);

  contractCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService'];

  function contractCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService) {
    var vm = this;
    vm.contractOpt = {
      depCon: {
        state: false,
        firstStep: false,
        secondStep: false
      },
      schCon: {
        state: false,
        firstStep: false,
        secondStep: false
      }
    };
    init();

    function init() {
      console.log($rootScope.userRole);
      if ($rootScope.userRole !== 'admin') {
        $state.go('notFound');
      }
      // vm.departID = $stateParams.departID;  // this is gonna be department id
      BEService.call('getWelcome')
        .then(function(res) {
          $rootScope.loading = false;
          $rootScope.showContent = true;
        })
        .catch(function() {
          toastr.error('Something went wrong', 'Error');
        });
        $timeout(function() {
          $('.ui.secondary.pointing.menu .item').tab();
        }, 100);
    }

  }
})();
