(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.contracts-gallery', {
      url: '/contracts',
      templateUrl: 'modules/management/contract/contract.html',
      controller: 'contractCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-contract', {
      url: '/contract/:contractID',
      templateUrl: 'modules/management/contract/custom-contract.html',
      controller: 'contractCtrl as ctrl',
      data: {}
    }).state('dashboard.management.add-contract', {
      url: '/newcontract',
      templateUrl: 'modules/management/contract/contract-process.html',
      controller: 'contractCtrl as ctrl',
      data: {}
    });
  }
})();
