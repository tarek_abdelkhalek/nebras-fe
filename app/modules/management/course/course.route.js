(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.course', { 
      url: '/course',
      templateUrl: 'modules/management/course/templates/course.html',
      controller:'courseCtrl as ctrl',
      data: {}
    }).state('dashboard.management.add-course', {
      url: '/newcourse',
      templateUrl: 'modules/management/course/templates/course-process.html',
      controller: 'courseCtrl as ctrl',
      data: {}
    });
  }
})();
