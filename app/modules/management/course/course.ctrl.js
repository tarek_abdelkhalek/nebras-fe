 (function () {
   'use strict';

   angular.module('app.learn')
     .controller('courseCtrl', courseCtrl);

   courseCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', 'toastr', 'BEService'];

   function courseCtrl($rootScope, $stateParams, $timeout, toastr, BEService) {
     var vm = this;
     vm.course = {
       newCourse: {
         selectedCategory: '',
         selectedCourse: '',
         selectedSubject: ''
       },
       courseInfo: {
         name: '',
         desc: '',
         requirements: '',
         outline: '',
         lesson: {
           title: '',
           desc: '',
           link: '',
           note: ''
         },
         lessonList: []
       },
       steps: {
         fStep: true,
         sStep: false,
         tStep: false
       }
     };
     vm.addLesson = addLesson;


     init();

     function init() {
       $rootScope.loading = false;
       $rootScope.showContent = true;
       //jquery code 
       $timeout(function () {
         $('.ui.radio.checkbox').checkbox();
       }, 100);
     }
     /**
      * forming lessons array
      */
     function addLesson() {
       var lesson = {
         title: vm.course.courseInfo.lesson.title,
         desc: vm.course.courseInfo.lesson.desc,
         link: vm.course.courseInfo.lesson.link,
         note: vm.course.courseInfo.lesson.note
       };
       vm.course.courseInfo.lessonList.push(lesson);
       vm.course.courseInfo.lesson.title = '';
       vm.course.courseInfo.lesson.desc = '';
       vm.course.courseInfo.lesson.link = '';
       vm.course.courseInfo.lesson.note = '';
     }
   }
 })();
