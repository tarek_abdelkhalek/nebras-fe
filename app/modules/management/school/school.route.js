(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.schools-gallery', {
      url: '/schools',
      templateUrl: 'modules/management/school/templates/school.html',
      controller: 'schoolCtrl as ctrl',
      data: {}
    }).state('dashboard.management.schools-statistics', {
      url: '/schoolstatistics',
      templateUrl: 'modules/management/school/templates/school-statistics.html',
      controller: 'schoolCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-school', {
      abstract: true,
      url: '/school/:schoolID',
      templateUrl: 'modules/management/school/templates/custom-school.html',
      data: {}
    }).state('dashboard.management.custom-school.info', {
      url: '/info',
      templateUrl: 'modules/management/school/templates/school-info.html',
      controller: 'schoolCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-school.student', {
      url: '/students',
      templateUrl: 'modules/management/school/templates/school-student.html',
      controller: 'schoolCtrl as ctrl',
      data: {}
    }).state('dashboard.management.schools-manage', {
      url: '/newschool',
      templateUrl: 'modules/management/school/templates/school-manage.html',
      controller: 'schoolCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.schUpdate = false;
        }]
      }
    }).state('dashboard.management.editsch', {
      url: '/school/:schoolID/edit',
      templateUrl: 'modules/management/school/templates/school-manage.html',
      controller: 'schoolCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.schUpdate = true;
        }]
      }
    });
  }
})();
