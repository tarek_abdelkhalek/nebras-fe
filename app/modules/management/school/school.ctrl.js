(function () {
  'use strict';

  angular.module('app.management')
    .controller('schoolCtrl', schoolCtrl);

  schoolCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService', 'NgTableParams'];

  function schoolCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService, NgTableParams) {
    var vm = this;
    vm.adminStatistics = {
      studentActivity: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        series: ['Series A', 'Series B'],
        data: [
          [65, 59, 80, 81, 56, 55, 40],
          [28, 48, 40, 19, 86, 27, 90]
        ],
        datasetOverride: [{
          yAxisID: 'y-axis-1'
        }, {
          yAxisID: 'y-axis-2'
        }],
        options: {
          scales: {
            yAxes: [{
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left'
              },
              {
                id: 'y-axis-2',
                type: 'linear',
                display: true,
                position: 'right'
              }
            ]
          }
        },
      }
    };
    vm.schools = {
      tableParams: new NgTableParams({
        count: 10
      }, {
        counts: [],
        getData: fetchSchool
      }),
      customSchool: {}
    };
    vm.schoolManage = {
      profile: {
        name: '',
        address: '',
        phone: '',
        picture: 'assets/images/dep.jpg',
        department_id: null,
        office_id: null
      },
      contacts: {
        state: false,
        edit: false,
        content: {
          name: '',
          email: '',
          phone: '',
          position: '',
        },
        contactList: []
      },
      availableDeparts: [],
      availableOffices: []
    };

    vm.manageSchoolProcess = manageSchoolProcess;
    vm.checkContactStatus = checkContactStatus;
    vm.discardNewContact = discardNewContact;
    vm.addNewContact = addNewContact;
    vm.editContact = editContact;
    vm.showModal = showModal;
    init();

    function init() {
      // check access permission
      if ($rootScope.userRole !== 'admin' && $rootScope.userRole !== 'department' &&
        $rootScope.userRole !== 'office') {
        $state.go('notFound');
      }
      if ($stateParams.schoolID) {
        //fetch school info
        if ($rootScope.isState('dashboard.management.custom-school.info')) {
          BEService.call('getCustomSchool', {
              id: $stateParams.schoolID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              console.log(res.data);
              $rootScope.currentSchoolID = res.data.id;
              vm.schools.customSchool.info = res.data;
              console.log(vm.schools.customSchool.info);
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'خطأ');
            });

          // fetch students in specific school
        } else if ($rootScope.isState('dashboard.management.custom-school.student')) {
          vm.schools.customSchool.studentTable = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomSchool
          });
          // update sepecific school
        } else if ($rootScope.isState('dashboard.management.editsch')) {
          BEService.call('getCustomSchool', {
              id: $stateParams.schoolID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.schoolManage.profile = res.data;
              vm.schoolManage.contacts.contactList = res.data.contacts;
              if ($rootScope.userRole === 'admin') {
                BEService.call('departCompact')
                  .then(function (res) {
                    vm.schoolManage.availableDeparts = res.data;
                  })
                  .catch(function (err) {
                    toastr.error('حدث خطأ ما', 'خطأ');
                  });
                BEService.call('officeCompact')
                  .then(function (res) {
                    vm.schoolManage.availableOffices = res.data;
                  })
                  .catch(function (err) {
                    toastr.error('حدث خطأ ما', 'خطأ');
                  });
              }
              if ($rootScope.userRole === 'department') {
                console.log('here');
                BEService.call('officeCompact')
                  .then(function (res) {
                    vm.schoolManage.availableOffices = res.data;
                    console.log(vm.schoolManage.availableOffices);
                  })
                  .catch(function (err) {
                    toastr.error('حدث خطأ ما', 'خطأ');
                  });
              }
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
        }
        // create new school
      } else if ($rootScope.isState('dashboard.management.schools-manage')) {
        if ($rootScope.userRole === 'admin') {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          BEService.call('departCompact')
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.schoolManage.availableDeparts = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
          BEService.call('officeCompact')
            .then(function (res) {
              vm.schoolManage.availableOffices = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
        } else if ($rootScope.userRole === 'department') {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          BEService.call('officeCompact')
            .then(function (res) {
              console.log(res.data);
              vm.schoolManage.availableOffices = res.data;
              $rootScope.loading = false;
              $rootScope.showContent = true;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
        } else {
          $rootScope.loading = false;
          $rootScope.showContent = true;
        }
      }
      //jquery code
      $timeout(function () {
        $('.ui.secondary.pointing.menu .item').tab();

        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#school-logo-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
          }
        }
        $('#school-logo').change(function () {
          readURL(this);
        });
        $('#school-manage-form')
          .form({
            fields: {
              schName: {
                identifier: 'schName',
                rules: [{
                  type: 'empty',
                  prompt: 'اسم الإدارة مطلوب'
                }]
              },
              schoolPhone: {
                identifier: 'schoolPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              vm.manageSchoolProcess();
            }
          });

        $('#manage-school-contact')
          .form({
            fields: {
              contactPosition: {
                identifier: 'contactPosition',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الوظيفة.'
                }]
              },
              contactName: {
                identifier: 'contactName',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الاسم.'
                }]
              },
              contactMail: {
                identifier: 'contactMail',
                optional: true,
                rules: [{
                  type: 'email',
                  prompt: 'من فضلك ارفق بريد الكتروني صحيح.'
                }]
              },
              contactPhone: {
                identifier: 'contactPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              if (vm.schoolManage.contacts.edit) {
                console.log('edit contact');
                vm.editContact();
              } else {
                console.log('new contact');
                vm.addNewContact();
              }
            }
          });
      }, 200);
    }

    /**
     *  fetching schools
     * @param {*} params
     */
    function fetchSchool(params) {
      $rootScope.loading = true;
      $rootScope.showContent = true;
      return BEService.call('getSchool', {
          size: params.count(),
          page: params.page(),
          sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          return res.data.docs;
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }
    /**
     * fetch custom school
     * @param {*} params 
     */
    function fetchCustomSchool(params) {
      $rootScope.loading = true;
      $rootScope.showContent = false;
      return BEService.call('getCustomSchool', {
          params: {
            size: params.count(),
            page: params.page(),
            sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
          },
          url: {
            id: $stateParams.schoolID,
            state: $state.current.url.slice(1)
          }
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          return res.data.docs;
        })
        .catch(function (err) {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }

    /**
     * 
     */
    function manageSchoolProcess() {
      $rootScope.loading = true;
      var payload = new FormData();
      if (vm.schoolManage.profile.picture) {
        if (angular.isString(vm.schoolManage.profile.picture)) {
          delete vm.schoolManage.profile.picture;
        } else {
          for (var key in vm.schoolManage.profile.picture) {
            if (vm.schoolManage.profile.picture.hasOwnProperty(key)) {
              var file = vm.schoolManage.profile.picture[key];
              vm.schoolManage.profile.picture = file;
            }
          }
        }
      }
      payload = vm.schoolManage.profile;
      console.log(payload);
      // school update
      if ($rootScope.schUpdate) {
        BEService.call('updateSchool', {
            id: payload.id
          }, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.loading = false;
            toastr.success('تم تعديل المكتب بنجاح', 'نجاح');
            $rootScope.progress = false;
            vm.schoolManage.contacts.state = true;
          })
          .catch(function () {
            toastr.error('Something went wrong', 'خطأ');
          });

      } else {
        // add school
        BEService.call('addSchool', null, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.justCreatedSchId = res.data.id;
            toastr.success('تم اضافة المكتب بنجاح', 'نجاح');
            $rootScope.progress = false;
            vm.schoolManage.contacts.state = true;
          })
          .catch(function () {
            toastr.error('حدث خطأ ما', 'خطأ');
          });
      }
    }
    /**
     * hide modal for
     * addign new contact
     */
    function discardNewContact() {
      $('.school-contact-manage').modal('hide');
      $state.reload();
    }
    /**
     * add new contact for 
     * specific department
     */
    function addNewContact() {
      $rootScope.loading = true;
      var payload = vm.schoolManage.contacts.content;
      if ($rootScope.justCreatedSchId) {
        payload.school_id = $rootScope.justCreatedSchId;
      } else {
        payload.school_id = $stateParams.schoolID;
      }
      vm.discardNewContact();
      BEService.call('addContact', null, payload)
        .then(function (res) {
          $rootScope.loading = false;
          vm.schoolManage.contacts.contactList.push(res.data);
          toastr.success('تم اضافة معلومات تواصل بنجاح', 'نجاح');
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }
    /**
     * edit existing contact for 
     * specific department
     */
    function editContact() {
      $rootScope.loading = true;
      var payload = vm.schoolManage.contacts.content;
      if ($rootScope.justCreatedSchId) {
        payload.school_id = $rootScope.justCreatedSchId;
      } else {
        payload.school_id = $stateParams.schoolID;
      }
      vm.discardNewContact();
      BEService.call('editContact', {
          id: payload.id
        }, payload)
        .then(function (res) {
          $rootScope.loading = false;
          $state.reload();
          toastr.success('تم تعديل معلومات تواصل بنجاح', 'نجاح');
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }
    /**
     * check office info and 
     * contacts active status 
     */
    function checkContactStatus() {
      if ($rootScope.schUpdate === true) {
        if (vm.schoolManage.contacts.state === false) {
          vm.schoolManage.contacts.state = true;
        } else {
          vm.schoolManage.contacts.state = false;
        }
      }
    }
    /**
     * show modal for
     * addign new contact
     */
    function showModal(contact) {
      if (contact) {
        vm.schoolManage.contacts.edit = true;
        vm.schoolManage.contacts.content = contact;
      }
      $('.school-contact-manage')
        .modal({
          closable: false,
        })
        .modal('show');
    }
  }
})();
