(function () {
  'use strict';

  angular.module('app.management')
    .controller('studentCtrl', studentCtrl);

  studentCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService', 'NgTableParams'];

  function studentCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService, NgTableParams) {
    var vm = this;
    vm.adminStatistics = {
      studentActivity: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        series: ['Series A', 'Series B'],
        data: [
          [65, 59, 80, 81, 56, 55, 40],
          [28, 48, 40, 19, 86, 27, 90]
        ],
        datasetOverride: [{
          yAxisID: 'y-axis-1'
        }, {
          yAxisID: 'y-axis-2'
        }],
        options: {
          scales: {
            yAxes: [{
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left'
              },
              {
                id: 'y-axis-2',
                type: 'linear',
                display: true,
                position: 'right'
              }
            ]
          }
        },
      }
    };
    vm.students = {
      tableParams: new NgTableParams({
        count: 10
      }, {
        counts: [],
        getData: fetchStudents
      }),
      customStudent: {}
    };
    vm.studentManage = {
      profile: {
        name: '',
        address: '',
        phone: '',
        academic_division: '',
        academic_year: null,
        picture: 'assets/images/dep.jpg',
        department_id: null,
        school_id: null,
        office_id: null
      },
      contacts: {
        state: false,
        edit: false,
        content: {
          name: '',
          email: '',
          phone: '',
          position: '',
        },
        contactList: []
      },
      availableDeparts: [],
      availableOffices: [],
      availableSchools: []
    };
    vm.manageStudentProcess = manageStudentProcess;
    vm.laterFunc = laterFunc;
    init();

    function init() {
      if ($rootScope.userRole !== 'admin' && $rootScope.userRole !== 'department' &&
        $rootScope.userRole !== 'office' && $rootScope.userRole !== 'school') {
        $state.go('notFound');
      }
      if ($stateParams.studentID) {
        if ($rootScope.isState('dashboard.management.custom-student.info')) {
          BEService.call('getCustomStudent', {
              id: $stateParams.studentID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.students.customStudent.info = res.data;
              $rootScope.currentStudenID = res.data.id;
              console.log(vm.students.customStudent);
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'Error');
            });
        } else if ($rootScope.isState('dashboard.management.editstud')) {
          BEService.call('getCustomStudent', {
              id: $stateParams.studentID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              console.log(res.data);

              vm.studentManage.profile = res.data;
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'Error');
            });
        }
      } else {
        if ($rootScope.userRole === 'admin') {
          BEService.call('departCompact')
            .then(function (res) {
              console.log(res.data);
              vm.studentManage.availableDeparts = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
          BEService.call('officeCompact')
            .then(function (res) {
              console.log(res.data);
              vm.studentManage.availableOffices = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
          BEService.call('schoolCompact')
            .then(function (res) {
              console.log(res.data);
              vm.studentManage.availableSchools = res.data;
              $rootScope.loading = false;
              $rootScope.showContent = true;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
        }
      }
      //jquery code
      $timeout(function () {
        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#student-logo-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
          }
        }
        $('#student-logo').change(function () {
          readURL(this);
        });
        $('#student-manage-form')
          .form({
            fields: {
              studName: {
                identifier: 'studName',
                rules: [{
                  type: 'empty',
                  prompt: 'اسم الإدارة مطلوب'
                }]
              },
              academic: {
                identifier: 'academicYear',
                rules: [{
                  type: 'empty',
                  prompt: 'السنة الاكاديمية مطلوبة'
                }]
              },
              academicDivision: {
                identifier: 'academicDivision',
                rules: [{
                  type: 'empty',
                  prompt: 'القسم مطلوب'
                }]
              },
              studePhone: {
                identifier: 'studePhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              vm.manageStudentProcess();
            }
          });
      }, 200);
    }
    /**
     *  fetching Students
     * @param {*} params
     */
    function fetchStudents(params) {
      $rootScope.loading = true;
      $rootScope.showContent = false;
      return BEService.call('getStudent', {
          size: params.count(),
          page: params.page(),
          sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          console.log(res.data);
          return res.data.docs;
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'Error');
        });
    }
    /**
     * manage student 
     * where to update 
     * and add students.
     */
    function manageStudentProcess() {
      $rootScope.loading = true;
      var payload = new FormData();
      if (vm.studentManage.profile.picture) {
        if (angular.isString(vm.studentManage.profile.picture)) {
          delete vm.studentManage.profile.picture;
        } else {
          for (var key in vm.studentManage.profile.picture) {
            if (vm.studentManage.profile.picture.hasOwnProperty(key)) {
              var file = vm.studentManage.profile.picture[key];
              vm.studentManage.profile.picture = file;
            }
          }
        }
      }
      payload = vm.studentManage.profile;
      if (payload.academic_division === null) {
        delete payload.academic_division;
      }
      if ($rootScope.studUpdate) {
        BEService.call('updateStudent', {
            id: payload.id
          }, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.progress = false;
            $rootScope.loading = false;
            $state.reload();
            toastr.success('تم تعديل ملف الطالب بنجاح', 'نجاح');
          })
          .catch(function () {
            toastr.error('Something went wrong', 'خطأ');
            $rootScope.progress = false;
          });

      } else {
        console.log(payload);
        BEService.call('addStudent', null, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.progress = false;
            $rootScope.loading = false;
            $state.go('dashboard.management.students-gallery');
            toastr.success('تم اضافة الطالب بنجاح', 'نجاح');
          })
          .catch(function () {
            $rootScope.progress = false;
            toastr.error('حدث خطأ ما', 'خطأ');
          });
      }
    }

    /** 
     * this function is going to be 
     * a to do later func '
     * to handle select dep,sch,and office
     * partitioning 
     */
    function laterFunc(ele) {
      // if ($rootScope.userRole === 'admin') {
      //   BEService.call('departCompact')
      //     .then(function (res) {
      //       console.log(res.data);
      //     })
      //     .catch(function (err) {
      //       toastr.error('حدث خطأ ما', 'خطأ');
      //     });
      // }
      console.log(ele);
    }
  }
})();
