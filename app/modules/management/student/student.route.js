(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.students-gallery', {
      url: '/students',
      templateUrl: 'modules/management/student/templates/student.html',
      controller: 'studentCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-student', {
      abstract: true,
      url: '/student/:studentID',
      templateUrl: 'modules/management/student/templates/custom-student.html',
      data: {}
    }).state('dashboard.management.custom-student.info', {
      url: '/info',
      templateUrl: 'modules/management/student/templates/student-info.html',
      controller: 'studentCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-student.tutorial', {
      url: '/tutorials',
      templateUrl: 'modules/management/student/templates/student-tutorial.html',
      controller: 'studentCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-student.exam', {
      url: '/exams',
      templateUrl: 'modules/management/student/templates/student-exam.html',
      controller: 'studentCtrl as ctrl',
      data: {}
    }).state('dashboard.management.students-manage', {
      url: '/newstudent',
      templateUrl: 'modules/management/student/templates/student-manage.html',
      controller: 'studentCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.studUpdate = false;
        }]
      }
    }).state('dashboard.management.editstud', {
      url: '/student/:studentID/edit',
      templateUrl: 'modules/management/student/templates/student-manage.html',
      controller: 'studentCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.studUpdate = true;
        }]
      }
    });
  }
})();
