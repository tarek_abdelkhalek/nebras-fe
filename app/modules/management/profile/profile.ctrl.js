 (function () {
   'use strict';

   angular.module('app.learn')
     .controller('profileCtrl', profileCtrl);

   profileCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService'];

   function profileCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService) {
     var vm = this;
     //vars
     vm.profileInput = [{
       id: 1,
       label: 'الاسم',
       name: 'name',
       type: 'text',
       model: ''
     }, {
       id: 2,
       label: 'العنوان',
       name: 'address',
       type: 'text',
       model: ''
     }, {
       id: 3,
       label: 'عدد المدارس',
       name: 'num_of_schools',
       type: 'number',
       model: ''
     }, {
       id: 4,
       label: 'الرقم الارضي',
       name: 'landline',
       type: 'text',
       model: ''
     }, ];
     vm.fieldView = [];
     //methods
     vm.getStarted = getStarted;
     vm.updateProfile = updateProfile;
     vm.updateLogo = updateLogo;
     init();

     function init() {
       vm.getStarted($rootScope.profile);
       $rootScope.loading = false;
       $rootScope.showContent = true;
       //jquery code
       $timeout(function () {
         $('#overall-progress').progress();
       }, 100);
     }
     /**
      * get started for initalizing 
      * profile data inputs array.
      * @param {*} userInputs 
      */
     function getStarted(userInputs) {
       for (var row in userInputs) {
         if (row === 'id' || row === 'created_at' ||
           row === 'picture' || row === 'updated_at') {
           continue;
         } else {
           for (var i = 0; i < vm.profileInput.length; i++) {
             if (row === vm.profileInput[i].name) {
               vm.profileInput[i].model = userInputs[row];
               vm.fieldView.push(vm.profileInput[i]);
             }
           }
         }
       }
     }
     /**
      * submit function to 
      * update profile info.
      */
     function updateProfile() {
       $rootScope.loading = true;
       $rootScope.showContent = false;
       var payload = {};
       for (var i = 0; i < vm.fieldView.length; i++) {
         payload[vm.fieldView[i].name] = vm.fieldView[i].model;
       }
       BEService.call('updateProfile', null, payload)
         .then(function () {
           $rootScope.loading = false;
           $rootScope.showContent = true;
           toastr.success('تم تعديل البيانات بنجاح');
         })
         .catch(function (err) {
           toastr.error('حدث خطأ ما');
         });
     }

     function updateLogo() {
       $rootScope.loading = true;
       var payload = new FormData();
       if (vm.files) {
         for (var key in vm.files) {
           if (vm.files.hasOwnProperty(key)) {
             var file = vm.files[key];
             payload.append('picture', file);
           }
         }
         console.log(vm.files);
       }
       BEService.call('updateProfile', null, payload, function (e) {
           $rootScope.progress = e.loaded * 100 / e.total;
           console.log(vm.progress);
         })
         .then(function (res) {
           $rootScope.loading = false;
           $state.reload();
           toastr.success('تم تعديل البيانات بنجاح');
           $rootScope.progress = false;
         })
         .catch(function () {
           toastr.error('Something went wrong', 'Error');
         });
     }
   }
 })();
