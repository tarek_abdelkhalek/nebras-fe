(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.profile', {
      abstract: true,
      url: '/profile',
      templateUrl: 'modules/management/profile/templates/profile.html',
      data: {}
    }).state('dashboard.management.profile.basic', { 
      url: '/basic',
      templateUrl: 'modules/management/profile/templates/profile-basic.html',
      controller: 'profileCtrl as ctrl',
      data: {}
      }).state('dashboard.management.profile.access', {
        url: '/access',
        templateUrl: 'modules/management/profile/templates/profile-access.html',
        controller: 'profileCtrl as ctrl',
        data: {}
      }).state('dashboard.management.profile.contract', {
        url: '/contract',
        templateUrl: 'modules/management/profile/templates/profile-contract.html',
        controller: 'profileCtrl as ctrl',
        data: {}
      });
  }
})();
