(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.bank', {
      url: '/question-bank',
      templateUrl: 'modules/management/bank/templates/bank.html',
      controller: 'bankCtrl as ctrl',
      data: {}
    }).state('dashboard.management.add-bank', {
      url: '/newbank',
      templateUrl: 'modules/management/bank/templates/bank-process.html',
      controller: 'bankCtrl as ctrl',
      data: {}
    }).state('dashboard.management.bank-manage', {
      url: '/courses/:courseID',
      templateUrl: 'modules/management/bank/templates/bank-manage.html',
      controller: 'bankCtrl as ctrl',
      data: {}
    });
  }
})();
