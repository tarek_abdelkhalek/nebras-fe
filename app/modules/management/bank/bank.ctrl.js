 (function () {
   'use strict';

   angular.module('app.learn')
     .controller('bankCtrl', bankCtrl);

   bankCtrl.$inject = [
     '$rootScope',
     '$stateParams',
     '$timeout',
     'toastr',
     'BEService',
     'NgTableParams',
     'env',
     '$http',
     '$state'
   ];

   function bankCtrl($rootScope, $stateParams, $timeout, toastr, BEService, NgTableParams, env, $http, $state) {
     var vm = this;
     vm.bank = {
       newBank: {
         selectedCourse: null,
         questionFormat: {
           header: '',
           choices: [],
         },
         correct: false,
         questionList: []
       },
       availableCourse: [],
       steps: {
         fStep: true,
         sStep: false
       },
       editor: {
         model: '',
         titleEdit: false,
         ansEdit: false
       }
     }; // root var end 
     $rootScope.selecteAnsID = null;

     vm.showModal = showModal;
     vm.addQuesTitle = addQuesTitle;
     vm.addSuggAns = addSuggAns;
     vm.deleteQuest = deleteQuest;
     vm.saveNewQuest = saveNewQuest;
     vm.moveToQuestion = moveToQuestion;
     vm.deleteSuggestedAns = deleteSuggestedAns;
     vm.discardNewQuest = discardNewQuest;
     vm.updateQuestion = updateQuestion;
     vm.updateAnswer = updateAnswer;
     init();

     function init() {
       $rootScope.loading = true;
       $rootScope.showContent = false;


       if ($rootScope.isState('dashboard.management.add-bank')) {
         BEService.call('getCoursesCompact')
           .then(function (res) {
             vm.bank.availableCourse = res.data;
             $rootScope.loading = false;
             $rootScope.showContent = true;
           })
           .catch(function () {
             toastr.error('Something went wrong', 'Error');
           });
       } else if ($rootScope.isState('dashboard.management.bank-manage')) {
         vm.bank.availableCourse = new NgTableParams({
           count: 10
         }, {
           counts: [],
           getData: fetchBank
         });
       } else {
         BEService.call('getCoursesCompact')
           .then(function (res) {
             vm.bank.availableCourse = res.data;
             $rootScope.loading = false;
             $rootScope.showContent = true;
           })
           .catch(function () {
             toastr.error('Something went wrong', 'Error');
           });
       }
     }

     function moveToQuestion(id) {
       vm.bank.steps.fStep = false;
       vm.bank.steps.sStep = true;
       vm.bank.newBank.selectedCourse = id;
     }

     /**
      * add question title 
      */
     function addQuesTitle() {
       vm.bank.newBank.questionFormat.header = vm.bank.editor.model;
       vm.bank.editor.model = '';
     }
     /**
      * add suggest answer 
      */
     function addSuggAns() {
       if (vm.bank.editor.model !== '') {
         vm.bank.newBank.questionFormat.choices.push({
           'content': vm.bank.editor.model,
           'correct': false
         });
         vm.bank.editor.model = '';
       }
     }

     /**
      * add new quetion to question bank 
      */
     function saveNewQuest() {
       vm.discardNewQuest();
       $rootScope.loading = true;
       vm.bank.newBank.questionList.push(vm.bank.newBank.questionFormat);
       console.log(vm.bank.newBank.questionFormat);
       $http({
         method: 'POST',
         url: env.apiUrl + '/courses/' + vm.bank.newBank.selectedCourse + '/questions',
         data: vm.bank.newBank.questionFormat
       }).then(function (res) {
         $rootScope.loading = false;
         toastr.success('تم اضافة  السؤال بنجاح', 'نجاح');
         $state.go('dashboard.management.bank-manage', {
           courseID: vm.bank.newBank.selectedCourse
         });
       }).catch(function (err) {
         toastr.error(err.message);
         console.log(err.message);
       });
     }
     /**
      * display new question modal
      */
     function showModal(row) {
       if (row) {
         console.log(row);
         vm.bank.newBank.questionFormat = row;
       }

       $('.ui.fullscreen.longer.modal')
         .modal({
           closable: false,
         })
         .modal('show');
     }
     /**
      * discard new question changes 
      * and hide modal
      */
     function discardNewQuest() {
       $('.ui.fullscreen.longer.modal').modal('hide');
     }

     function fetchBank(params) {
       $rootScope.loading = true;
       $rootScope.showContent = false;
       return BEService.call('getQuestionBank', {
           params: {
             size: params.count(),
             page: params.page(),
             sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
           },
           url: {
             id: $stateParams.courseID,
           }
         })
         .then(function (res) {
           params.total(res.data.pagination.row_count);
           $rootScope.loading = false;
           $rootScope.showContent = true;
           console.log(res.data);
           return res.data.docs;
         })
         .catch(function () {
           toastr.error('حدث خطأ ما', 'Error');
         });
     }

     function deleteSuggestedAns(row) {
       console.log('delete process');
       console.log(row);
       $rootScope.loading = true;
       $rootScope.showContent = false;
       if (row) {
         //  vm.bank.newBank.questionFormat.choices.splice(index, 1);
         BEService.call('deleteQuestionBank', row.id)
           .then(function () {
             toastr.success('تم مسح الاختيار بنجاح', 'نجاح');
             $rootScope.loading = false;
             $rootScope.showContent = true;
           })
           .catch(function () {
             toastr.error('حدث خطأ ما', 'خطأ');
           });
       }
     }

     function updateQuestion() {
       if (!vm.bank.editor.titleEdit) {
         vm.bank.editor.titleEdit = true;
         vm.bank.editor.model = vm.bank.newBank.questionFormat.header;
       } else {
         vm.bank.newBank.questionFormat.header = vm.bank.editor.model;
         vm.bank.editor.titleEdit = false;
         vm.bank.editor.model = '';
         BEService.call('patchQuestionBank', vm.bank.newBank.questionFormat.id, {
             header: vm.bank.newBank.questionFormat.header
           })
           .then(function (res) {
             toastr.success('تم تعديل السؤال بنجاح', 'نجاح');
           })
           .catch(function () {
             toastr.error('حدث خطأ ما', 'خطأ');
           });
       }
     }

     function deleteQuest(row) {
       $rootScope.loading = true;
       $rootScope.showContent = false;
       if (row) {
         //  vm.bank.newBank.questionFormat.choices.splice(index, 1);
         BEService.call('deleteQuestionBank', row.id)
           .then(function () {
             toastr.success('تم مسح السؤال بنجاح', 'نجاح');
             $rootScope.loading = false;
             $rootScope.showContent = true;
           })
           .catch(function () {
             toastr.error('حدث خطأ ما', 'خطأ');
           });
       }
     }

     function updateAnswer(ans, index) {

       if (!vm.bank.editor.ansEdit) {
         $rootScope.selecteAnsID = index;
         vm.bank.editor.ansEdit = true;
         vm.bank.editor.model = ans.content;
         $('#editans' + index).css('display', 'none');
         $('#finishans' + index).css('display', 'inline-block');
       } else {
         if ($rootScope.selecteAnsID !== index) {
           return;
         } else {
           $rootScope.loading = true;
           $rootScope.showContent = false;
           $('#finishans' + index).css('display', 'none');
           $('#editans' + index).css('display', 'inline-block');
           vm.bank.newBank.questionFormat.choices[index].content = vm.bank.editor.model;
           vm.bank.editor.ansEdit = false;
           vm.bank.editor.model = '';
           BEService.call('patchAnswerBank', ans.id, {
               content: vm.bank.newBank.questionFormat.choices[index].content
             })
             .then(function (res) {

               $rootScope.loading = false;
               $rootScope.showContent = true;
               toastr.success('تم تعديل الاختيار بنجاح', 'نجاح');
             })
             .catch(function () {
               toastr.error('حدث خطأ ما', 'خطأ');
             });
         }

       }
     }
   }
 })();
