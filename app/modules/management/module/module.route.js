(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];
  function routes($stateProvider) {
    $stateProvider.state('dashboard.management', {
      abstract: true,
      url: '/management',
      templateUrl: 'modules/management/module/module.html',
      controller: 'ModuleCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$auth', '$q', function($auth, $q) {
          var authPayload = $auth.getPayload();
          if(typeof authPayload === 'undefined' ||
              typeof authPayload.userRole === 'undefined' ||
            authPayload.userRole !== 'admin' && authPayload.userRole !== 'department'&&  
            authPayload.userRole !== 'office' && authPayload.userRole !== 'school' ) {
            return $q.reject({ code: 404 });
          }
        }]
      }
    });
  }
})();
