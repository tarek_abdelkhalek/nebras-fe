(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.offices-gallery', {
      url: '/offices',
      templateUrl: 'modules/management/office/templates/office.html',
      controller: 'officeCtrl as ctrl',
      data: {}
    }).state('dashboard.management.offices-statistics', {
      url: '/officestatistics',
      templateUrl: 'modules/management/office/templates/office-statistics.html',
      controller: 'officeCtrl as ctrl',
      data: {}
    }).state('dashboard.management.offices-manage', {
      url: '/office-manage',
      templateUrl: 'modules/management/office/templates/manage-office.html',
      controller: 'officeCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.offUpdate = false;
        }]
      }
    }).state('dashboard.management.editoff', {
      url: '/office/:officeID/edit',
      templateUrl: 'modules/management/office/templates/manage-office.html',
      controller: 'officeCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.offUpdate = true;
        }]
      }
    }).state('dashboard.management.custom-office', {
      abstract: true,
      url: '/office/:officeID',
      templateUrl: 'modules/management/office/templates/custom-office.html',
      data: {}
    }).state('dashboard.management.custom-office.info', {
      url: '/info',
      templateUrl: 'modules/management/office/templates/office-info.html',
      controller: 'officeCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-office.school', {
      url: '/schools',
      templateUrl: 'modules/management/office/templates/office-school.html',
      controller: 'officeCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-office.student', {
      url: '/students',
      templateUrl: 'modules/management/office/templates/office-student.html',
      controller: 'officeCtrl as ctrl',
      data: {}
    });
  }
})();
