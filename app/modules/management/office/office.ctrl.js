(function () {
  'use strict';

  angular.module('app.management')
    .controller('officeCtrl', officeCtrl);

  officeCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService', 'NgTableParams'];

  function officeCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService, NgTableParams) {
    var vm = this;
    vm.adminStatistics = {
      all: {
        labels: ['مدرسة', 'طالب'],
        data: [30, 70],
        type: 'polarArea'
      },
      offices: {
        labels: ['مكتب السلام', 'مكتب الابراهيميه', 'مكتب العوايد', 'مكت كرموز', 'مكتب الورديان', 'مكتب سيدي بشر'],
        data: [70, 80, 90, 75, 85, 95],
        colors: ['#14284D', '#31B7D0', '#EB6001', '#FBD416', '#F7464A']
      },
      schools: {
        labels: ['إدارة شرق', 'إدارة غرب', 'إدارة وسط', 'إدارة العامرية'],
        data: [10, 20, 15, 12],
        colors: ['#14284D', '#31B7D0', '#EB6001', '#FBD416', '#F7464A']
      },
      studentActivity: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        series: ['Series A', 'Series B'],
        data: [
          [65, 59, 80, 81, 56, 55, 40],
          [28, 48, 40, 19, 86, 27, 90]
        ],
        datasetOverride: [{
          yAxisID: 'y-axis-1'
        }, {
          yAxisID: 'y-axis-2'
        }],
        options: {
          scales: {
            yAxes: [{
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left'
              },
              {
                id: 'y-axis-2',
                type: 'linear',
                display: true,
                position: 'right'
              }
            ]
          }
        },
      }
    };
    vm.offices = {
      tableParams: new NgTableParams({
        count: 10
      }, {
        counts: [],
        getData: fetchOffices
      }),
      customOffice: {}
    };
    vm.officeManage = {
      profile: {
        name: '',
        address: '',
        num_of_schools: null,
        phone: '',
        picture: 'assets/images/dep.jpg',
        department_id: null
      },
      contacts: {
        state: false,
        edit: false,
        content: {
          name: '',
          email: '',
          phone: '',
          position: '',
        },
        contactList: []
      },
      availableDeparts: []
    };

    //start methods
    vm.showModal = showModal;
    vm.editContact = editContact;
    vm.addNewContact = addNewContact;
    vm.discardNewContact = discardNewContact;
    vm.checkContactStatus = checkContactStatus;
    vm.manageOfficeProcess = manageOfficeProcess;
    init();

    function init() {
      // check access role
      if ($rootScope.userRole !== 'admin' && $rootScope.userRole !== 'department') {
        $state.go('notFound');
      }
      if ($stateParams.officeID) {
        $rootScope.loading = true;
        if ($rootScope.isState('dashboard.management.custom-office.info')) {
          BEService.call('getCustomOffice', {
              id: $stateParams.officeID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.offices.customOffice.info = res.data;
              $rootScope.currentOffID = res.data.id;
              console.log(res.data);
              if (vm.offices.customOffice.info.picture === null) {
                vm.offices.customOffice.info.picture = 'assets/images/dep.jpg';
              }
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
        } else if ($rootScope.isState('dashboard.management.editoff')) {
          BEService.call('getCustomOffice', {
              id: $stateParams.officeID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.officeManage.profile = res.data;
              vm.officeManage.contacts.contactList = res.data.contacts;
              if (vm.officeManage.profile.picture === null) {
                vm.officeManage.profile.picture = 'assets/images/dep.jpg';
              }
            })
            .catch(function () {
              toastr.error('حدث خطأ ما', 'خطأ');
            });
          if ($rootScope.userRole === 'admin') {
            BEService.call('departCompact')
              .then(function (res) {
                console.log(res.data);
                vm.officeManage.availableDeparts = res.data;
              })
              .catch(function (err) {
                toastr.error('حدث خطأ ما', 'Error');
              });
          }
        } else if ($rootScope.isState('dashboard.management.custom-office.school')) {
          vm.offices.customOffice.officeTable = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomOffice
          });
        } else if ($rootScope.isState('dashboard.management.custom-office.student')) {
          vm.offices.customOffice.studentTable = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomOffice
          });
        }
      } else if ($rootScope.isState('dashboard.management.offices-manage')) {
        if ($rootScope.userRole === 'admin') {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          BEService.call('departCompact')
            .then(function (res) {
              console.log(res.data);
              $rootScope.loading = false;
              $rootScope.showContent = true;
              vm.officeManage.availableDeparts = res.data;
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'Error');
            });
        } else {
          $rootScope.loading = false;
          $rootScope.showContent = true;
        }
      }
      // jquery code
      $timeout(function () {
        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#office-logo-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
          }
        }
        $('#office-logo').change(function () {
          readURL(this);
        });
        $('.ui.secondary.pointing.menu .item').tab();

        $('#office-manage-form')
          .form({
            fields: {
              offName: {
                identifier: 'offName',
                rules: [{
                  type: 'empty',
                  prompt: 'اسم المكتب مطلوب'
                }]
              },
              schoolPhone: {
                identifier: 'schoolPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              vm.manageOfficeProcess();
            }
          });


        $('#manage-off-contacts')
          .form({
            fields: {
              contactPosition: {
                identifier: 'contactPosition',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الوظيفة.'
                }]
              },
              contactName: {
                identifier: 'contactName',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الاسم.'
                }]
              },
              contactMail: {
                identifier: 'contactMail',
                optional: true,
                rules: [{
                  type: 'email',
                  prompt: 'من فضلك ارفق بريد الكتروني صحيح.'
                }]
              },
              contactPhone: {
                identifier: 'contactPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              if (vm.officeManage.contacts.edit) {
                vm.editContact();
              } else {
                vm.addNewContact();
              }
            }
          });



      }, 250);
    }

    /**
     *  fetching offices
     * @param {*} params
     */
    function fetchOffices(params) {
      $rootScope.loading = true;
      $rootScope.showContent = true;
      return BEService.call('getOffice', {
          size: params.count(),
          page: params.page(),
          sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          return res.data.docs;
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'Error');
        });
    }
    /**
     * fetch custom office info 
     * schools and students
     * @param {*} params 
     */
    function fetchCustomOffice(params) {
      $rootScope.loading = true;
      $rootScope.showContent = false;
      return BEService.call('getCustomOffice', {
          params: {
            size: params.count(),
            page: params.page(),
            sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
          },
          url: {
            id: $stateParams.officeID,
            state: $state.current.url.slice(1)
          }
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          console.log(res.data);
          return res.data.docs;
        })
        .catch(function (err) {
          toastr.error('حدث خطأ ما', 'Error');
        });
    }
    /**
     * manage office through 
     * create or update 
     * office
     */
    function manageOfficeProcess() {
      $rootScope.loading = true;
      var payload = new FormData();
      if (vm.officeManage.profile.picture) {
        if (angular.isString(vm.officeManage.profile.picture)) {
          delete vm.officeManage.profile.picture;
        } else {
          for (var key in vm.officeManage.profile.picture) {
            if (vm.officeManage.profile.picture.hasOwnProperty(key)) {
              var file = vm.officeManage.profile.picture[key];
              vm.officeManage.profile.picture = file;
            }
          }
        }
      }
      if (!angular.isNumber(vm.officeManage.profile.num_of_schools)) {
        delete vm.officeManage.profile.num_of_schools;
      }
      payload = vm.officeManage.profile;
      console.log(payload);
      if ($rootScope.offUpdate) {
        BEService.call('updateOffice', {
            id: payload.id
          }, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.loading = false;
            toastr.success('تم تعديل المكتب بنجاح', 'نجاح');
            vm.officeManage.contacts.state = true;
            $rootScope.progress = false;
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });

      } else {
        BEService.call('addOffice', null, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.justCreatedOffId = res.data.id;
            toastr.success('تم اضافة المكتب بنجاح', 'نجاح');
            vm.officeManage.contacts.state = true;
            $rootScope.progress = false;
          })
          .catch(function () {
            toastr.error('حدث خطأ ما', 'خطأ');
          });
      }
    }
    /**
     * show modal for
     * addign new contact
     */
    function showModal(contact) {
      if (contact) {
        vm.officeManage.contacts.content = contact;
        vm.officeManage.contacts.edit = true;
      }
      $('.contact-off-modal')
        .modal({
          closable: false,
        })
        .modal('show');
    }
    /**
     * hide modal for
     * addign new contact
     */
    function discardNewContact() {
      $('.contact-off-modal').modal('hide');
      $state.reload();
    }
    /**
     * add new contact for 
     * specific department
     */
    function addNewContact() {
      $rootScope.loading = true;
      var payload = vm.officeManage.contacts.content;
      if ($rootScope.justCreatedOffId) {
        payload.office_id = $rootScope.justCreatedOffId;
      } else {
        payload.office_id = $stateParams.officeID;
      }
      $('.contact-off-modal').modal('hide');
      BEService.call('addContact', null, payload)
        .then(function (res) {
          $rootScope.loading = false;
          vm.officeManage.contacts.contactList.push(res.data);
          toastr.success('تم اضافة معلومات تواصل بنجاح', 'نجاح');
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }
    /**
     * edit existing contact for 
     * specific department
     */
    function editContact() {
      $rootScope.loading = true;
      var payload = vm.officeManage.contacts.content;
      if ($rootScope.justCreatedOffId) {
        payload.office_id = $rootScope.justCreatedOffId;
      } else {
        payload.office_id = $stateParams.officeID;
      }
      $('.contact-off-modal').modal('hide');
      BEService.call('editContact', {
          id: payload.id
        }, payload)
        .then(function (res) {
          $rootScope.loading = false;
          $state.reload();
          toastr.success('تم تعديل معلومات تواصل بنجاح', 'نجاح');
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'خطأ');
        });
    }

    /**
     * check office info and 
     * contacts active status 
     */
    function checkContactStatus() {
      if ($rootScope.offUpdate === true) {
        if (vm.officeManage.contacts.state === false) {
          vm.officeManage.contacts.state = true;
        } else {
          vm.officeManage.contacts.state = false;
        }
      }
    }
  }
})();
