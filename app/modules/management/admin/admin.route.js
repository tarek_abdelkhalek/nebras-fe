(function () {
  'use strict';

  angular.module('app.management')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('dashboard.management.departs-gallery', {
      url: '/departs',
      templateUrl: 'modules/management/admin/templates/depart.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.admin-statistics', {
      url: '/departstatistics',
      templateUrl: 'modules/management/admin/templates/admin-statistics.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.newdepart', {
      url: '/newdepartment',
      templateUrl: 'modules/management/admin/templates/depart-manage.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-depart', {
      abstract: true,
      url: '/depart/:departID',
      templateUrl: 'modules/management/admin/templates/custom-depart.html',
      data: {}
    }).state('dashboard.management.editdep', {
      url: '/depart/:departID/edit',
      templateUrl: 'modules/management/admin/templates/depart-manage.html',
      controller: 'adminCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', function ($rootScope) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          $rootScope.depUpdate = true;
        }]
      }
    }).state('dashboard.management.custom-depart.info', {
      url: '/info',
      templateUrl: 'modules/management/admin/templates/depart-info.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-depart.office', {
      url: '/offices',
      templateUrl: 'modules/management/admin/templates/depart-office.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-depart.school', {
      url: '/schools',
      templateUrl: 'modules/management/admin/templates/depart-school.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    }).state('dashboard.management.custom-depart.student', {
      url: '/students',
      templateUrl: 'modules/management/admin/templates/depart-student.html',
      controller: 'adminCtrl as ctrl',
      data: {}
    });
  }
})();
