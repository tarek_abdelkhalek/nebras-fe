(function () {
  'use strict';

  angular.module('app.management')
    .controller('adminCtrl', adminCtrl);

  adminCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', '$state', 'toastr', 'BEService', 'NgTableParams'];

  function adminCtrl($rootScope, $stateParams, $timeout, $state, toastr, BEService, NgTableParams) {
    var vm = this;
    vm.adminStatistics = {
      all: {
        labels: ['مكتب', 'مدرسة', 'طالب'],
        data: [10, 30, 70],
        type: 'polarArea'
      },
      departments: {
        labels: ['إدارة شرق', 'إدارة غرب', 'إدارة وسط', 'إدارة العامرية'],
        data: [10, 20, 15, 12],
        colors: ['#14284D', '#31B7D0', '#EB6001', '#FBD416', '#F7464A']
      },
      offices: {
        labels: ['مكتب السلام', 'مكتب الابراهيميه', 'مكتب العوايد', 'مكت كرموز', 'مكتب الورديان', 'مكتب سيدي بشر'],
        data: [70, 80, 90, 75, 85, 95],
        colors: ['#14284D', '#31B7D0', '#EB6001', '#FBD416', '#F7464A']
      },
      schools: {
        labels: ['إدارة شرق', 'إدارة غرب', 'إدارة وسط', 'إدارة العامرية'],
        data: [10, 20, 15, 12],
        colors: ['#14284D', '#31B7D0', '#EB6001', '#FBD416', '#F7464A']
      },
      studentActivity: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        series: ['إدارة غرب الاسكندرية', 'إدارة شرق الاسكندرية'],
        data: [
          [20, 30, 40, 30, 50, 60, 70],
          [40, 50, 60, 70, 60, 50, 40]
        ],
        datasetOverride: [{
          yAxisID: 'y-axis-1'
        }, {
          yAxisID: 'y-axis-2'
        }],
        options: {
          scales: {
            yAxes: [{
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left'
              },
              {
                id: 'y-axis-2',
                type: 'linear',
                display: true,
                position: 'right'
              }
            ]
          }
        },
      }
    };
    vm.departs = {
      tableParams: new NgTableParams({
        count: 10
      }, {
        counts: [],
        getData: fetchDeparts
      }),
      customDepart: {},
      newDep: {
        name: '',
        address: '',
        num_of_schools: null,
        phone: '',
        picture: 'assets/images/dep.jpg',
        contacts: {
          state: false,
          content: {
            name: '',
            email: '',
            phone: '',
            position: ''
          },
          contactList: [],
          edit: false
        }
      },
    };
    vm.departmentManage = departmentManage;
    vm.delDepartment = delDepartment;
    vm.discardNewContact = discardNewContact;
    vm.addNewContact = addNewContact;
    vm.showModal = showModal;
    vm.checkManageStatus = checkManageStatus;


    init();

    function init() {
      if ($rootScope.userRole !== 'admin') {
        $state.go('notFound');
      }
      if ($stateParams.departID) {
        if ($rootScope.isState('dashboard.management.custom-depart.info') ||
          $rootScope.isState('dashboard.management.editdep')) {
          BEService.call('getCustomDepart', {
              id: $stateParams.departID,
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              $rootScope.depUpdate = true;

              if ($rootScope.isState('dashboard.management.editdep')) {
                vm.departs.newDep = res.data;
                vm.departs.newDep.contacts.contactList = res.data.contacts;
              } else {
                vm.departs.customDepart.info = res.data;
                $rootScope.currentDepartID = res.data.id;
                if (vm.departs.customDepart.info.picture === null) {
                  vm.departs.customDepart.info.picture = 'assets/images/dep.jpg';
                }
                console.log(vm.departs.customDepart.info);
              }
            })
            .catch(function (err) {
              toastr.error('حدث خطأ ما', 'Error');
            });
        } else if ($rootScope.isState('dashboard.management.custom-depart.office')) {
          vm.departs.customDepart.office = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomDepart
          });
        } else if ($rootScope.isState('dashboard.management.custom-depart.school')) {
          vm.departs.customDepart.school = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomDepart
          });
        } else if ($rootScope.isState('dashboard.management.custom-depart.student')) {
          vm.departs.customDepart.student = new NgTableParams({
            count: 10
          }, {
            counts: [],
            getData: fetchCustomDepart
          });
        }
      } else {
        $rootScope.loading = false;
        $rootScope.showContent = true;
        $rootScope.depUpdate = false;
        vm.departs.newDep.picture = false;
      }
      //jquery code
      $timeout(function () {
        function readURL(input) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#logo-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
          }
        }
        $('#dep-logo').change(function () {
          readURL(this);
        });
        $('.ui.secondary.pointing.menu .item').tab();
        $('#new-department-form')
          .form({
            fields: {
              name: {
                identifier: 'department-name',
                rules: [{
                  type: 'empty',
                  prompt: 'اسم الإدارة مطلوب'
                }]
              },
              departPhone: {
                identifier: 'departPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              vm.departmentManage();
            }
          });
        $('#add-new-contact')
          .form({
            fields: {
              position: {
                identifier: 'position',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الوظيفة.'
                }]
              },
              contactName: {
                identifier: 'contactName',
                rules: [{
                  type: 'empty',
                  prompt: 'من فضلك ارفق الاسم.'
                }]
              },
              contactMail: {
                identifier: 'contactMail',
                optional: true,
                rules: [{
                  type: 'email',
                  prompt: 'من فضلك ارفق بريد الكتروني صحيح.'
                }]
              },
              contactPhone: {
                identifier: 'contactPhone',
                optional: true,
                rules: [{
                  type: 'regExp',
                  value: /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/,
                  prompt: 'ارفق رقم جوال صحيح',
                }]
              }
            },
            onSuccess: function () {
              vm.addNewContact();
            }
          });
      }, 100);
    }

    /**
     *  fetching departs
     * @param {*} params
     */
    function fetchDeparts(params) {
      $rootScope.loading = true;
      $rootScope.showContent = true;
      return BEService.call('getDepart', {
          size: params.count(),
          page: params.page(),
          sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          console.log(res.data.docs);
          return res.data.docs;
        })
        .catch(function () {
          toastr.error('حدث خطأ ما', 'Error');
        });
    }
    /**
     *  fetching departs
     * @param {*} params
     */
    function fetchCustomDepart(params) {
      $rootScope.loading = true;
      $rootScope.showContent = false;
      return BEService.call('getCustomDepart', {
          params: {
            size: params.count(),
            page: params.page(),
            sortBy: (params.orderBy()[0]) ? params.orderBy()[0] : '+id',
          },
          url: {
            id: $stateParams.departID,
            state: $state.current.url.slice(1)
          }
        })
        .then(function (res) {
          params.total(res.data.pagination.row_count);
          $rootScope.loading = false;
          $rootScope.showContent = true;
          return res.data.docs;
        })
        .catch(function (err) {
          toastr.error('حدث خطأ ما', 'Error');
        });
    }
    /**
     * add new department 
     */
    function departmentManage() {
      $rootScope.loading = true;
      var payload = new FormData();
      if (vm.departs.newDep.picture) {
        for (var key in vm.departs.newDep.picture) {
          if (vm.departs.newDep.picture.hasOwnProperty(key)) {
            var file = vm.departs.newDep.picture[key];
            vm.departs.newDep.picture = file;
          }
        }

      }
      if (vm.departs.newDep.num_of_schools === null || !angular.isNumber(vm.departs.newDep.num_of_schools)) {
        delete vm.departs.newDep.num_of_schools;
      }
      payload = vm.departs.newDep;
      if ($rootScope.depUpdate) {
        BEService.call('updateDepart', {
            id: payload.id
          }, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            $rootScope.loading = false;
            toastr.success('تم تعديل الإدارة بنجاح', 'نجاح');
            vm.departs.newDep.contacts.state = true;
            $rootScope.progress = false;
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      } else {
        BEService.call('addDepart', null, payload, function (e) {
            $rootScope.progress = e.loaded * 100 / e.total;
          })
          .then(function (res) {
            console.log(res.data);
            $rootScope.justCreatedDepartId = res.data.id;
            $rootScope.loading = false;
            toastr.success('تم اضافة الإدارة بنجاح', 'نجاح');
            vm.departs.newDep.contacts.state = true;
            $rootScope.progress = false;
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      }
    }
    /**
     * delete selected department
     * @param {*} id 
     * @param {*} name 
     */
    function delDepartment(id, name) {
      $('.small.modal').modal('show');
      vm.confirmation = {
        name: name,
        confirm: function () {
          $('.small.modal').modal('hide');
          BEService.call('delDepart', {
              id: id
            })
            .then(function (res) {
              toastr.success('تم مسح الإدارة بنجاح', 'نجاح');
              $state.reload('');
            })
            .catch(function () {
              toastr.error('Something went wrong', 'Error');
            });
        },
        cancel: function () {
          $('.small.modal').modal('hide');
        }
      };
    }
    /**
     * show modal for
     * addign new contact
     */
    function showModal(contact) {
      if (contact) {
        vm.departs.newDep.contacts.edit = true;
        vm.departs.newDep.contacts.content = contact;
      }
      $('.ui.modal.dep-contact')
        .modal({
          etachable: true,
          closable: false,
        });
      $('.ui.modal.dep-contact').modal('show');
    }
    /**
     * hide modal for
     * addign new contact
     */
    function discardNewContact() {
      $('.ui.modal.dep-contact').modal('hide');
    }
    /**
     * add new contact for 
     * specific department
     */
    function addNewContact() {
      $rootScope.loading = true;
      var payload = vm.departs.newDep.contacts.content;
      console.log(payload);
      if (vm.departs.newDep.contacts.edit === true) {
        vm.discardNewContact();
        BEService.call('editContact', {
            id: payload.id
          }, payload)
          .then(function (res) {
            $rootScope.loading = false;
            vm.departs.newDep.contacts.edit = false;
            toastr.success('تم تعديل معلومات تواصل بنجاح', 'نجاح');
          })
          .catch(function () {
            toastr.error('حدث خطأ ما', 'خطأ');
          });
      } else {
        if ($rootScope.justCreatedDepartId) {
          payload.department_id = $rootScope.justCreatedDepartId;
        } else {
          payload.department_id = $stateParams.departID;
        }
        vm.discardNewContact();
        BEService.call('addContact', null, payload)
          .then(function (res) {
            $rootScope.loading = false;
            vm.departs.newDep.contacts.contactList.push(res.data);
            toastr.success('تم اضافة معلومات تواصل بنجاح', 'نجاح');
          })
          .catch(function () {
            toastr.error('حدث خطأ ما', 'خطأ');
          });
      }
    }
    /**
     * toggle check to 
     * move between department info 
     * and department contacts
     */
    function checkManageStatus() {
      if ($rootScope.depUpdate) {
        vm.departs.newDep.contacts.state = !vm.departs.newDep.contacts.state ? true : false;
      } else {
        return;
      }
    }
  }
})();
