(function () {
  'use strict';

  angular.module('app.learn')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('account.learn.course', {
      url: '/courses',
      templateUrl: 'modules/learn/courses/templates/courses.html',
      controller: 'coursesCtrl as ctrl',
      data: {}
    }).state('account.learn.course-view', {
      url: '/course/:courseID',
      templateUrl: 'modules/learn/courses/templates/course-view.html',
      controller: 'coursesCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$rootScope', '$stateParams', 'BEService', function ($rootScope, $stateParams, BEService) {
          $rootScope.loading = true;
          $rootScope.showContent = false;
          BEService.call('getCustomCourse', {
              courseId: $stateParams.courseID
            })
            .then(function (res) {
              $rootScope.loading = false;
              $rootScope.showContent = true;
              $rootScope.viewedCourse = res.data;
              console.log(res.data);
            })
            .catch(function () {});
        }]
      }
    }).state('account.learn.course-view.info', {
      url: '/info',
      templateUrl: 'modules/learn/courses/templates/course-info.html',
      controller: 'coursesCtrl as ctrl',
      data: {}
      }).state('account.learn.course-view.video', {
      url: '/video/:videoID',
      templateUrl: 'modules/learn/courses/templates/course-video.html',
      controller: 'coursesCtrl as ctrl',
      data: {}
    });
  }
})();
