(function () {
  'use strict';

  angular.module('app.learn')
    .controller('coursesCtrl', coursesCtrl);

  coursesCtrl.$inject = ['$rootScope', '$stateParams', '$state', '$timeout', 'toastr', 'BEService'];

  function coursesCtrl($rootScope, $stateParams, $state, $timeout, toastr, BEService) {
    var vm = this;
    vm.courses = {
      viewedCourse: {},
      collecting: {},
      capabilities: {},
      video: {},
      currentCommentID: null
    };
    vm.buttonText = '';
    vm.videoStatus = videoStatus;
    vm.newCommentReply = newCommentReply;
    vm.newVideoComment = newVideoComment;
    vm.assginCommentID = assginCommentID;
    vm.moveToCourse = moveToCourse;
    init();

    function init() {
      $rootScope.loading = true;
      $rootScope.showContent = false;
      if ($rootScope.isState('account.learn.course-view.info') && $stateParams.courseID) {
        BEService.call('getCustomCourse', {
            courseId: $stateParams.courseID
          })
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.showContent = true;
            vm.courses.viewedCourse = res.data;
            console.log(res.data);
            vm.courses.viewedCourse.coverStyle = {
              'background-image': 'url(' + vm.courses.viewedCourse.cover + ')'
            };
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      } else if ($rootScope.isState('account.learn.course-view.video') && $stateParams.videoID) {
        BEService.call('getVideo', $stateParams.videoID)
          .then(function (res) {
            vm.courses.video = res.data;
            $rootScope.generalVideoID = vm.courses.video.id;
            console.log(vm.courses.video);
            BEService.call('getComments', $stateParams.videoID)
              .then(function (res) {
                $rootScope.loading = false;
                $rootScope.showContent = true;
                vm.courses.comments = res.data;
              })
              .catch(function () {
                toastr.error('Something went wrong', 'Error');
              });
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      } else {
        BEService.call('getCourses')
          .then(function (res) {
            vm.courses.collecting = res.data[0];
            vm.courses.capabilities = res.data[1];
            console.log(res.data);
            $rootScope.loading = false;
            $rootScope.showContent = true;
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      }
      // jquery injection
      $timeout(function () {
        $('#overall-progress').progress();
        $('.ui.embed').embed();
        $('.ui.secondary.pointing.menu .item').tab();
        $('.ui.tiny.dimmable.image').dimmer({
          on: 'hover'
        });
      }, 300);
    }

    /** 
     * toggle video button 
     * watch or un watched
     */
    function videoStatus() {
      if (vm.courses.video.completed) {
        BEService.call('unwatchedVideo', vm.courses.video.id)
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.showContent = true;
            $state.reload();
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      } else {
        BEService.call('watchedVideo', vm.courses.video.id)
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.showContent = true;
            $state.reload();
          })
          .catch(function () {
            toastr.error('Something went wrong', 'Error');
          });
      }
    }
    /**
     * post new comment for
     * specific video lesson.
     */
    function newVideoComment() {
      BEService.call('videoComment', vm.courses.video.id, {
          content: vm.newComment
        })
        .then(function (res) {
          $rootScope.loading = false;
          $rootScope.showContent = true;
          $state.reload();
        })
        .catch(function () {
          toastr.error('Something went wrong', 'Error');
        });
    }
    /**
     * post new comment for
     * specific video lesson.
     */
    function newCommentReply() {
      BEService.call('commentReply', vm.courses.currentCommentID, {
          content: vm.commentReply
        })
        .then(function (res) {
          $rootScope.loading = false;
          $rootScope.showContent = true;
          $state.reload();
        })
        .catch(function () {
          toastr.error('Something went wrong', 'Error');
        });
    }

    function assginCommentID(id) {
      vm.courses.currentCommentID = id;
    }

    function moveToCourse(id) {
      console.log(id);
      console.log('id');
      $rootScope.generalCourseID = id;
      $state.go('account.learn.course-view.info', {
        courseID: id
      });
    }
  }
})();
