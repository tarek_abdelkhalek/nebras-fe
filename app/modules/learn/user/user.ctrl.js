(function () {
  'use strict';

  angular.module('app.learn')
    .controller('userCtrl', userCtrl);

  userCtrl.$inject = ['$rootScope', '$stateParams', '$timeout', 'toastr', 'BEService'];

  function userCtrl($rootScope, $stateParams, $timeout, toastr, BEService) {
    var vm = this;

    init();

    function init() {
      // vm.departID = $stateParams.departID;  // this is gonna be department id
      $rootScope.loading = true;
      $rootScope.showContent = false;
      BEService.call('getCourses')
        .then(function (res) {
          $rootScope.loading = false;
          $rootScope.showContent = true;
        })
        .catch(function () {
          toastr.error('Something went wrong', 'Error');
        });
      $timeout(function () {
        $('.ui.secondary.pointing.menu .item').tab();
        $('#overall-progress').progress();
      }, 100);
    }
  }
})();
