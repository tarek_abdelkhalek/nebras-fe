(function () {
  'use strict';

  angular.module('app.learn')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('account.learn.user', {
      url: '/user',
      templateUrl: 'modules/learn/user/user.html',
      controller: 'userCtrl as ctrl',
      data: {}
    });
  }
})();
