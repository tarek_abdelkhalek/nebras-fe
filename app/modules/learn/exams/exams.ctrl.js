(function () {
  'use strict';

  angular.module('app.learn')
    .controller('examsCtrl', examsCtrl);

  examsCtrl.$inject = ['$rootScope', '$stateParams', '$state', '$timeout', 'toastr', 'BEService'];

  function examsCtrl($rootScope, $stateParams, $state, $timeout, toastr, BEService) {
    var vm = this;
    vm.selectedCourses = [];
    init();

    function init() {
      $rootScope.loading = true;
      $rootScope.showContent = false;

      if ($rootScope.isState('account.learn.exams')) {
        BEService.call('getExams')
          .then(function (res) {
            $rootScope.loading = false;
            $rootScope.showContent = true;
            console.log(res.data);
            vm.previousExams = res.data;
          })
          .catch(function (err) {
            toastr.error('خطأ', 'حدث خطأ ما');
          });
      } else if ($rootScope.isState('account.learn.newexam')) {
        BEService.call('getCoursesCompact')
          .then(function (res) {
            console.log(res.data);
            $rootScope.availableCourses = res.data;
            $rootScope.loading = false;
            $rootScope.showContent = true;
          })
          .catch(function (err) {
            toastr.error('خطأ', 'حدث خطأ ما');
          });
      }
      // jquery injection
      // $timeout(function () {
      //   $('#overall-progress').progress();
      //   $('.ui.embed').embed();
      //   $('.ui.secondary.pointing.menu .item').tab();
      //   $('.ui.tiny.dimmable.image').dimmer({
      //     on: 'hover'
      //   });
      // }, 300);
    }

    vm.tarek = function (index, id) {
      $('.shape' + index).shape('flip over');
      if (!vm.selectedCourses.includes(id)) {
        vm.selectedCourses.push(id);
      } else {
        vm.selectedCourses.splice(vm.selectedCourses.indexOf(id), 1);
      }
      console.log(vm.selectedCourses);
    };
  }
})();
