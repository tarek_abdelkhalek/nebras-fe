(function () {
  'use strict';

  angular.module('app.learn')
    .config(routes);

  routes.$inject = ['$stateProvider'];

  function routes($stateProvider) {
    $stateProvider.state('account.learn.exams', {
      url: '/exams',
      templateUrl: 'modules/learn/exams/templates/exams.html',
      controller: 'examsCtrl as ctrl',
      data: {}
    }).state('account.learn.newexam', {
      url: '/newexam',
      templateUrl: 'modules/learn/exams/templates/exams-new.html',
      controller: 'examsCtrl as ctrl',
      data: {}
    });
  }
})();
