(function () {
  'use strict';

  angular.module('app.learn')
    .config(routes);

  routes.$inject = ['$stateProvider'];
  function routes($stateProvider) {
    $stateProvider.state('account.learn', {
      abstract: true,
      url: '/learn',
      templateUrl: 'modules/learn/module/module.html',
      controller: 'ModuleCtrl as ctrl',
      data: {},
      resolve: {
        init: ['$auth', '$q', function($auth, $q) {
          var authPayload = $auth.getPayload();
          if(typeof authPayload === 'undefined' ||
              typeof authPayload.userRole === 'undefined' ||
              authPayload.userRole !== 'student') {
            return $q.reject({ code: 404 });
          }
        }]
      }
    });
  }
})();
