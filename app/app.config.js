(function () {
  'use strict';

  angular.module('app')
    .constant('env', window._env)
    .run(appRun)
    .config(appConfig)
    .factory('httpInterceptor', httpInterceptor);

  appRun.$inject = ['$rootScope', '$auth', '$state', '$window'];
  function appRun($rootScope, $auth, $state, $window) {
    $rootScope.$on('$stateChangeStart', onStateChange);
    $rootScope.$on('$stateChangeError', onStateError);
    $rootScope.isState = isState;
    $rootScope.progress = false;
    if ($auth.isAuthenticated()){
      $rootScope.userRole = $auth.getPayload().userRole;

    }
    function onStateChange(e, toState, toParams, fromState) {
      $rootScope.loading = true;        // show loading spinner while loading
      $rootScope.showContent = false;   // show content after loading

      var isProtected = toState.data && toState.data.protected;
      if(isProtected && !$auth.isAuthenticated()) {
        e.preventDefault();
        $state.go('login', {}, { reload: true });
      } else if(toState.name === 'login' && $auth.isAuthenticated()) {
        e.preventDefault();
        $window.location = '/';
      }
    }

    function onStateError(e, toState, toParams, fromState, fromParams, error) {
      if(error.code === 401) {
        $auth.logout();
        $state.go('login', {}, { reload: true });
      } else if(error.code === 404) {
        $state.go('notFound');
      }
    }

    function isState(state) {
      return ($state.current.name.indexOf(state) !== -1);
    }
  }

  appConfig.$inject = [
    'env',
    '$authProvider',
    '$urlRouterProvider',
    '$httpProvider',
    '$translateProvider',
    'toastrConfig'
  ];
  function appConfig(
    env,
    $authProvider,
    $urlRouterProvider,
    $httpProvider,
    $translateProvider,
    toastrConfig) {
    // satellizer
    $authProvider.loginUrl = env.apiUrl + '/login';
    $authProvider.tokenName = 'accessToken';
    $authProvider.tokenHeader = 'X-Access-Token';
    $authProvider.tokenPrefix = 'HZ';
    $authProvider.tokenType = '';

    // default routes
    $urlRouterProvider
      .when('', '/')
      .otherwise('/404');

    // if last charcter is a slash, return the same url without the slash
    $urlRouterProvider.rule(function ($injector, $location) {
      var path = $location.path();
      var hasTrailingSlash = path[path.length-1] === '/';
      if(hasTrailingSlash) {
        var newPath = path.substr(0, path.length - 1);
        return newPath;
      }
    });

    // http requests interceptors
    $httpProvider.interceptors.push('httpInterceptor');

    // translation
    $translateProvider
      .useStaticFilesLoader({ prefix: '/locales/locale-', suffix: '.json' })
      .preferredLanguage('en')
      .useSanitizeValueStrategy('sanitize');

    // toaster
    angular.extend(toastrConfig, {
      autoDismiss: true,
      closeButton: true,
      tapToDismiss: false,
      timeOut: 4000,
      extendedTimeOut: 0
    });
  }

  httpInterceptor.$inject = ['$injector', '$q', '$window'];
  function httpInterceptor($injector, $q, $window) {
    return {
      responseError: function (rejection) {
        var $auth = $injector.get('$auth');
        if(rejection.status === 401 && $auth.isAuthenticated()) {
          $auth.logout();
          $window.location = '/';
          return;
        }
        // handle ERR_CONNECTION_REFUSED
        // uncomplete http request
        // else if(rejection.status <= 0) {
        //   $auth.logout();
        //   $window.location = '/';
        //   return;
        // }
        return $q.reject(rejection);
      }
    };
  }

})();
