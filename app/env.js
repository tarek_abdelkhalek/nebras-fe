(function (window) {
  'use strict';

  var env = {};
  // env.backendBaseUrl = 'http://192.168.43.209:4000';
  env.backendBaseUrl = 'https://othmanapi.herokuapp.com';




  env.apiVersion = 'v1';
  env.apiUrl = env.backendBaseUrl + '/api/' + env.apiVersion;

  window._env = window._env || env;

}(this));
